package com.philips.stemcell.classification

import java.io.{File, FileOutputStream, FileWriter}
import java.nio.file.Paths
import java.util.UUID.randomUUID

import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.WriteConfig
import com.philips.stemcell.jieba.wordSp
import com.philips.stemcell.utilities.normalization
import ml.combust.bundle.BundleFile
import ml.combust.mleap.spark.SparkSupport._
import nlp.CRFModel
import org.apache.spark.ml.bundle.SparkBundleContext
import org.apache.spark.ml.classification.{MultilayerPerceptronClassificationModel, MultilayerPerceptronClassifier}
import org.apache.spark.ml.feature.{HashingTF, IDF, IndexToString, StringIndexer, Tokenizer}
import org.apache.spark.ml.linalg.{DenseVector, SparseVector, Vector}

import scala.collection.immutable.Map
import scala.util.control.Exception
//import org.apache.spark.mllib.linalg.SparseVector
//import org.apache.spark.ml.linalg.SparseVector
import org.apache.spark.ml.mleap.SparkUtil
import org.apache.spark.ml.{Pipeline, PipelineModel}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
import resource._
import java.io._
import scala.math.floor



class thyroidNumberClassifier(spark: SparkSession) extends normalization {
  def preProcessing(Annotations: DataFrame,
                    raw: DataFrame,
                    modelPath: String = "./modelfiles/thyroidNumberTransformer",
                    schema: String = "thyroidUS"
                   )= {

//    val textRaw = Annotations.select(col("quote"), col("text"),
//      col("tags").getItem(0).alias("tag"),
//      col("ranges").getItem(0)("endOffset").alias("end"),
//      col("ranges").getItem(0)("startOffset").alias("start"),
//      col("referer_url")
//    ).withColumn("length", col("end") - col("start"))

    val text = Annotations.filter(col("start") === -1
      && trim(col("tag")) === "root.结节数目"
      && trim(col("text")) =!= ""
    ).select("tag", "referer_url", "text")
    text.show()

    import spark.implicits._
    val text1 = text.rdd.map( x => {
      val md5 = x.getString(1).split("md5=")(1)
      val text = x.getString(2)
      (md5, text)
    }).toDF("md5", "text")

//    text1.show()
//    raw.show()
//    raw.printSchema()
//    val raw1 = new wordSp(spark).process(RawTexts, schema)
//    val raw1 = wordSp.run(sp)
    val step = raw.join(text1, raw.col("md5") === text1.col("md5"), "inner")
  .drop(raw.col("md5"))
      .select("raw_txt", "text", "md5").distinct()
    val step0 = new wordSp(spark).process(step, schema).select("raw_txt", "words", "text")
//    val step0R = raw.join(text1, raw.col("md5") === text1.col("md5"), "inner")
//      .select("raw_txt", "text").distinct()

    val stringIndex = new StringIndexer().setInputCol("text").setOutputCol("label")
    val step1 = stringIndex.fit(step0).transform(step0)

    /**
      * Save lookUp table for index to string revert
      */
    val lookUp = step1.select("text", "label").distinct()
      .rdd.map(x => {
      val text = x.getString(0)
      val label = x.getAs[Double](1)
      (text, label)
    }).collect()

    val outLayer = step1.select("text").distinct().count().toInt

//    lookUp.foreach(
//      println(_)
//    )

    val file = "./modelfiles/tcLookup.txt"
    val writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)))
    for (x <- lookUp) {
      writer.write(x._1 + " " + x._2 + "\n")
    }
    writer.close()


    println("Sentence count = " + step1.count().toString)
    step1.printSchema()
    step1.show()

//    val tokenizer = new Tokenizer().setInputCol("raw_txt").setOutputCol("words")
//
//    val wordsData = tokenizer.transform(step1)
//    val wordsDataR= tokenizer.transform(step0R)

//    wordsData.printSchema()
//    wordsDataR.printSchema()
//    wordsData.select("words").show(false)
//    wordsDataR.select("words").show(false)

    /**
      * Constructing data pre-processing pipeline
      */
    val hashingTF = new HashingTF()
      .setInputCol("words").setOutputCol("rawFeatures").setNumFeatures(500)

//    val featurizedData = hashingTF.transform(wordsData)
    // alternatively, CountVectorizer can also be used to get term frequency vectors

    val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
//    val idfModel = idf.fit(featurizedData)


//    val rescaledData = idfModel.transform(featurizedData)
//    rescaledData.select("text", "features").show()

    val pipeline = new Pipeline()
      .setStages(Array(hashingTF, idf))
    val transformerPipeline = pipeline.fit(step1)

    transformerPipeline.write.overwrite().save(modelPath)

    /**
      * Preprocess input data using pipeline
      */
    val train = transformerPipeline.transform(step1).select("features", "label")
    train.groupBy("label").count().show()


    /**
      * Auto re-balancing the label distribution
      */
    val dfCount = train.count()
    val balance1 = train.groupBy("label")
      .count()
      .withColumn("times", lit(dfCount)/col("count"))
      .select("label", "times").distinct()
      .rdd.map(x => {
      val label = x.getAs[Double](0)
      val times = floor(x.getAs[Double](1)).toInt
      val timeList = List.fill(times)("foo")
      (label, timeList)
    }).toDF("label", "times")
    val balance2 = train.join(balance1, balance1.col("label") === train.col("label"))
        .drop(balance1.col("label"))
        .withColumn("times", explode(col("times")))
        .drop("times")
    balance2.groupBy("label").count().show()

    println("Data pre process is completed")
    (balance2, step1, transformerPipeline, outLayer)
  }

  def training(train: DataFrame, step1:DataFrame,
               transformerPipeline: PipelineModel,
               outLayer: Int,
               modelName: String = "thyroidNumberMLP") = {
    import spark.sqlContext.implicits._
    val featureRow = train.select("features").head

    val inputLayer = featureRow(0).asInstanceOf[SparseVector].size

    println("Inputlayer size is "+ inputLayer.toString)

    train.show()

    /**
      * Set initial weights to train the model if possible
      */

    val model =
    try {
      val mlp = new MultilayerPerceptronClassifier()
        .setLayers(Array(inputLayer, 500, 500, outLayer))
        .setMaxIter(100)
      mlp.setInitialWeights(MultilayerPerceptronClassificationModel.load(s"./modelfiles/$modelName").weights)
      println("Training the model")
      mlp.fit(train)
    }
    catch {case _: Exception =>
      println("No initial weights can be set, MLP model trained from scratch")
      val mlp = new MultilayerPerceptronClassifier()
        .setLayers(Array(inputLayer, 500, 500, outLayer))
        .setMaxIter(100)
      mlp.fit(train)
    }


//    val evaluator = new MulticlassClassificationEvaluator().setMetricName("f1").setLabelCol("sub_flg")

    val weights = model.weights
    model.write.overwrite().save(s"./modelfiles/$modelName")
    println(s"$modelName has been saved")
    val prediction = model.transform(train)

//    val revert = new IndexToString().setInputCol("label").setOutputCol("predictionLabel")
//    val predictionL = revert.transform(prediction)
//    println("predictionL dataframe show")
//    predictionL.show()
//    val tp = prediction.filter("label = 1.0 and prediction = 1").count()
//    val tn = prediction.filter("label = 0.0 and prediction = 0").count()
//    val fp = prediction.filter("label = 0.0 and prediction = 1").count()
//    val fn = prediction.filter("label = 1.0 and prediction = 0").count()
//    val total = prediction.count()
//    val recall = tp.toFloat/(tp + fn)
//    val precision = tp.toFloat / (tp + fp)
//    val accuracy = (tp + tn).toFloat / total
//    val f1 = (2*(recall * precision)).toFloat / (recall + precision)
//
//
//    println("True Positives:", tp)
//    println("True Negatives:", tn)
//    println("False Positives:", fp)
//    println("False Negatives:", fn)
//    println("Recall", recall)
//    println("Precision", precision)
//    println("Accuracy", accuracy)
//    println("F1", f1)
//    println("Total", total)
//
//    println(model.extractParamMap())

    /**
      * Serialize Spark model and save Mleap file
      */
    val pipeline = SparkUtil.createPipelineModel(uid = "pipeline", Array(transformerPipeline, model))
    val sbc = SparkBundleContext().withDataset(pipeline.transform(step1))
    val currentDir = Paths.get(".").toAbsolutePath
    println(currentDir)
//    pipeline.write.overwrite().save(s"$currentDir/modelfiles/tumorFNN1.zip")
//    val fnnVersion = System.nanoTime().toString
    new File(s"./modelfiles/$modelName.zip").delete()
    for(bf <- managed(BundleFile(s"jar:file:$currentDir/modelfiles/$modelName.zip"))) {
      pipeline.writeBundle.save(bf)(sbc).get
    }

    println("Mleap bundle has been saved")
    (model, weights)
  }

  def prediction(data: DataFrame, model: MultilayerPerceptronClassificationModel, transformer: PipelineModel)={
    val results = transformer.transform(data).select("features")
    val results1 = model.transform(results)
    results1.show(false)
  }

  def pseudoLabeling(
                      RawTexts: DataFrame,
//                      mongoURI: String = "mongodb://localhost",
                      mongoURI: String = "mongodb://130.147.249.29",
//                      hostIP: String = "http://localhost",
                      hostIP: String = "mongodb://130.147.249.29",
                      modelName: String = "thyroidNumberMLP",
                      transformerName: String = "thyroidNumberTransformer"
                    ) = {

    val transformer = PipelineModel.load(s"./modelfiles/$transformerName")
    val mlp = MultilayerPerceptronClassificationModel.load(s"./modelfiles/$modelName")

    val raw1 = new wordSp(spark).process(RawTexts.withColumn("text", lit("0")), "thyroidUS")
    val raw2 = transformer.transform(raw1).select("features", "md5")
    val predict = mlp.transform(raw2)
    predict.show()
    predict.printSchema()
    import spark.implicits._

    val tcLookUp = scala.io.Source.fromFile("./modelfiles/tcLookup.txt").getLines().filter(_.nonEmpty)
      .map(_.split(" "))
      .map(x => (x(0), x(1))).toList

    val predict1 = predict.rdd.map(x => {
      val confidence = x.getAs[DenseVector](3).values.max
      val predictionRaw = x.getAs[Double](4).toString
//      val prediction = if (predictionRaw == 1.0) "单结节" else "多结节"
//      val prediction = if (tcLookUp.map(_._2).contains(predictionRaw))
//        tcLookUp.filter(x => {
//          x._2 == predictionRaw
//        }).map(_._1).head else "NA"
      val revert = new normalization().revertStringIndex(predictionRaw, tcLookUp)
      val prediction = revert
      val md5 = x.getString(1)
      (md5, prediction, confidence)
    }).toDF("md5", "prediction", "confidence")
      .filter(col("confidence") > 0.8)
    predict1.show()

    val predict2 = predict1.rdd.map( x => {
      val element = "root.结节数目"
      //        val tags: WrappedArray[String] = WrappedArray.make(Array(element))
      val tags = Seq(element)
      val quote = ""
      val ranges = Seq(("/div[2]", "/div[2]", "-1", "-1"))
      val md5 = x.getString(0)
      val referer_url = s"$hostIP:6543/iCMC/thinking/test?src=websql&schema=thyroidUS&md5=$md5"
      val annotator_schema_version = "v1.0"
      //        val norm = spark.sparkContext.broadcast(normalization(quote, element))
      val text = x.getString(1)
      val element_id = element
      val schema = "thyroidUS"
      val id = "stemCell"+randomUUID().toString
      (tags, quote, ranges, referer_url, annotator_schema_version, text, element_id, schema, id)
    }).toDF("tags", "quote", "ranges", "referer_url", "annotator_schema_version", "text", "element_id", "schema", "id")

    val rangesSchema = "array<struct<start:string,end:string,startOffset:integer,endOffset:integer>>"
    val predict3 = predict2.withColumn("ranges", col("ranges").cast(rangesSchema))
    val writeConfig = WriteConfig(Map("uri" -> mongoURI,"database" -> "AnnotatorJS", "collection" -> "plThyroidNumClassification", "writeConcern.w" -> "majority"), Some(WriteConfig(spark)))

    MongoSpark.save(predict3.write.mode("overwrite"), writeConfig)

    val predict4 = predict3.select(
      col("quote"),
      col("tags").getItem(0).alias("tag"),
      col("ranges").getItem(0)("endOffset").alias("end"),
      col("ranges").getItem(0)("startOffset").alias("start"),
      col("referer_url"),
      col("text")
    ).withColumn(
      "length", col("end") - col("start")
    )

    predict4.show()
    predict4

  }
}
