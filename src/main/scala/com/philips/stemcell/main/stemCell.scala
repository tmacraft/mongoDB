package com.philips.stemcell.main

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.{ReadConfig, WriteConfig}
import com.philips.stemcell.classification.thyroidNumberClassifier
import com.philips.stemcell.crf.stemCellCRF
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.{col, trim}
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.concurrent.duration.Duration

object stemCell {

  def main(args: Array[String]): Unit = {

    Logger.getLogger("org").setLevel(Level.WARN)

    val actorSystem = ActorSystem()
    val scheduler = actorSystem.scheduler

    implicit val executor: ExecutionContextExecutor = actorSystem.dispatcher

    val task = new Runnable {
      def run(): Unit = {
        try {
          val (rawOperation, rawThyroid, atOperation, atThyroid, spark) = stemCell.loadData()
          println("Data has been loaded")

//          batchUpload(spark)

//          val dLDA = new documentsLDA(spark)
//          val df = dLDA.preProcess(dfTumor, raw)
//          val ldaResult = dLDA.training(df)
//          println("LDA training is completed")
//          dLDA.writeMongo(ldaResult)

          /**
            * CRF training first round
            */
          val crfThyroid = new stemCellCRF(spark)
          val trainThyroid = crfThyroid.preProcessing(rawThyroid, atThyroid)
          println("CRF data pre process is completed")
          println("Training the CRF model...")
          val (crfThyroidModel, crfThyroidAccuracy) = crfThyroid.training(trainThyroid)
          println(s"crfThyroidAccuracy: $crfThyroidAccuracy")
          crfThyroid.saveCRFModel(crfThyroidModel, crfThyroidAccuracy, "crfThyroid")

          /**
            * MLP training first round
            */
          val tc = new thyroidNumberClassifier(spark)
          if (
            atThyroid
              .filter(
                col("start") === -1
                  && trim(col("tag")) === "root.结节数目"
                  && trim(col("text")) =!= ""
              )
              .select("text")
              .distinct()
              .rdd.map(_.getString(0))
              .collect()
              .length > 1
          ) {
            val (train, step1, transformerPipeline, outLayer) = tc.preProcessing(atThyroid, rawThyroid)
            println("Classification data pre process is completed")
            tc.training(train, step1, transformerPipeline, outLayer)
          } else println("Not enough label to trigger classification training")

          /**
            * CRF training second round
            */
          val pseudoLabelThyroid = crfThyroid.pseudoLabeling(rawThyroid, "thyroidUS")
          println("pseudoLabeling has been completed")
          val thyroidCombined = pseudoLabelThyroid.union(atThyroid).distinct()

          val trainThyroidPseudo = crfThyroid.preProcessing(rawThyroid, thyroidCombined)
          val (crfThyroidModelCombined, crfThyroidCombinedAccuracy) = crfThyroid.training(trainThyroidPseudo)
          crfThyroid.saveCRFModel(crfThyroidModelCombined, crfThyroidCombinedAccuracy, "crfThyroid")
          println(s"crfThyroidCombinedAccuracy: $crfThyroidCombinedAccuracy")

          /**
            * MLP training second round
            */
          if (
            atThyroid
              .filter(
                col("start") === -1
                  && trim(col("tag")) === "root.结节数目"
                  && trim(col("text")) =!= ""
              )
              .select("text")
              .distinct()
              .rdd.map(_.getString(0))
              .collect()
              .length > 1
          ) {
            val tcPseudo = tc.pseudoLabeling(rawThyroid)
            val tcCombined = tcPseudo.union(atThyroid).distinct()
            val (trainCombined, step2, transformerPipelinePL, outLayerPL) = tc.preProcessing(tcCombined, rawThyroid)
            tc.training(trainCombined, step2, transformerPipelinePL, outLayerPL)
          } else println("Not enough label to trigger classification training")

          println("Model has been updated")
        } catch {
          case _: Exception => println("Model update has failed")
        }
      }
    }


    scheduler.schedule(
      initialDelay = Duration(5, TimeUnit.SECONDS),
      interval = Duration(3, TimeUnit.MINUTES),
      runnable = task)

  }

  def loadData(
                operationSchema: String = "hccOperation",
                thyroidSchema: String = "thyroidUS",
                uriName: String = "mongodb://130.147.249.29",
//                uriName: String = "mongodb://localhost",
                dbName: String = "AnnotatorJS",
                collName: String = "RawTexts",
                collName2: String = "Annotations"
              ): (DataFrame, DataFrame, DataFrame, DataFrame, SparkSession) = {


    val spark = SparkSession.builder()
      .master("local[*]")
      .config("spark.driver.memory", "12g")
      .appName("stemCell")
      .config("spark.mongodb.input.uri", s"mongodb://130.147.249.29/AnnotatorJS.Annotations")
      .config("spark.mongodb.output.uri", "mongodb://130.147.249.29/AnnotatorJS.Annotations")
      .getOrCreate()

    val readConfigVal = ReadConfig(Map("uri" -> uriName,"database" -> dbName, "collection" -> collName, "readPreference.name" -> "secondaryPreferred"), Some(ReadConfig(spark)))
    val readConfigVal2 = ReadConfig(Map("uri" -> uriName,"database" -> dbName, "collection" -> collName2, "readPreference.name" -> "secondaryPreferred"), Some(ReadConfig(spark)))

    val rawOperation = MongoSpark.load(spark, readConfigVal).filter(col("schema_name") === operationSchema).distinct()
    val rawThyroid = MongoSpark.load(spark, readConfigVal)
      .distinct()
      .filter(col("schema_name") === thyroidSchema)
    val dfOperation = MongoSpark.load(spark, readConfigVal2).filter(col("schema") === operationSchema).distinct()
    val dfThyroidRaw = MongoSpark.load(spark, readConfigVal2).filter(col("schema") === thyroidSchema).distinct()

    /**
      * Extract fields from annotation dataframe
      */

    val dfThyroid = dfThyroidRaw.select(
      col("quote"),
      col("tags").getItem(0).alias("tag"),
      col("ranges").getItem(0)("endOffset").alias("end"),
      col("ranges").getItem(0)("startOffset").alias("start"),
      col("referer_url"),
      col("text")
    ).withColumn(
      "length", col("end") - col("start")
    )

    //    val raw = spark.read.json("./data/raw.json")
    //    val dfOperation = spark.read.json("./data/labeled.json").filter(col("schema") === operationSchema)
    //    val dfTumor = spark.read.json("./data/labeled.json").filter(col("schema") === tumorSchema)
    println("dfThyroid schema:")
    dfThyroid.printSchema()
    println("dfThyroid dataframe show:")
    dfThyroid.show()
    (rawOperation, rawThyroid, dfOperation, dfThyroid, spark)
  }

  def batchUpload(spark: SparkSession,
                  filePath: String = "./modelfiles/stemCell.csv",
//                  uriName: String = "mongodb://130.147.249.29",
                  uriName: String = "mongodb://localhost",
                  dbName: String = "AnnotatorJS",
                  collName: String = "RawTexts"
                 ) = {
    val batch = spark.read.option("header", true).option("inferSchema", true).csv(filePath)
    batch.show()
    import spark.implicits._
    val batch1 = batch.rdd.map( x => {
      val raw_txt = x.getString(0)
      val md5 = md5HashString(raw_txt)
      val schema_name = "thyroidUS"
      val topic = -1
      (raw_txt, md5, schema_name, topic)
    }).toDF("raw_txt", "md5", "schema_name", "topic")
    batch1.show()
    val writeConfig = WriteConfig(Map("uri" -> uriName,"database" -> dbName, "collection" -> collName, "writeConcern.w" -> "majority"), Some(WriteConfig(spark)))
    MongoSpark.save(batch1.write.option("collection", collName).mode("append"), writeConfig)
  }

  def md5HashString(s: String): String = {
    import java.security.MessageDigest
    import java.math.BigInteger
    val md = MessageDigest.getInstance("MD5")
    val digest = md.digest(s.getBytes)
    val bigInt = new BigInteger(1,digest)
    val hashedString = bigInt.toString(16)
    hashedString
  }

}
