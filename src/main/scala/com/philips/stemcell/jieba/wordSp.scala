package com.philips.stemcell.jieba

import com.huaban.analysis.jieba.JiebaSegmenter
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}
//import org.apache.spark.{SparkConf, SparkContext}

class wordSp(spark: SparkSession) {

  def process(RawTexts: DataFrame, schema: String) = {

    import spark.implicits._
    RawTexts.printSchema()
    val df = RawTexts
//      .filter(col("schema_name") === schema)
      .select("raw_txt", "text", "md5")
      .rdd.map { x =>
        val raw_txt = x.getString(0)
        val str = if (raw_txt.length > 0)
          new JiebaSegmenter().sentenceProcess(raw_txt)
        val text = x.getString(1)
        val md5 = x.getString(2)
        (raw_txt, str.toString.split(","), text, md5)
      }.toDF("raw_txt", "words", "text", "md5")

    df
  }
}

