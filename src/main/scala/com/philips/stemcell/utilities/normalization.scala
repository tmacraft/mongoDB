package com.philips.stemcell.utilities


import org.apache.spark.sql.SparkSession

import scala.util.matching.Regex

class normalization {

  def normalization(text: String, element_id: String): String = {
    element_id match {
      case "root.钙化" => calcificationTransformer(text)
      case "root.TI-RADS评分" => radsTransformer(text)
      case "root.结节部位_侧" => noduleSideTransformer(text)
      case "root.结节部位_部" => nodulePartTransformer(text)
      case "root.结节部位_位" => noduleAreaTransformer(text)
      case "root.桥本氏甲状腺炎" => "有"
      case "root.结节性甲状腺肿" => "有"
      case _ => text
    }
  }

  def calcificationTransformer(text: String): String = {
    if (text contains "无") "无" else "有"
  }

  def radsTransformer(text: String): String = {
    val keys = List(
      "NA", "0", "1", "2", "3",
      "4", "4A", "4B", "4C", "5", "6"
    )
   val r1 = for (key <- keys) yield {
     text.toUpperCase.intersect(key)
   }

    val r2 = keys.intersect(r1)

    if (r2.isEmpty) "NA" else r2.last
  }

  def noduleSideTransformer(text: String) = {
    if (text.contains("左")) "左侧"
    else if (text.contains("右")) "右侧"
    else if (text.contains("双")) "双侧"
    else "NA"
  }

  def nodulePartTransformer(text: String) = {
    if (text.contains("中上")) "中上部"
    else if (text.contains("中下")) "中下部"
    else if (text.contains("中")) "中部"
    else if (text.contains("上")) "上部"
    else if (text.contains("下")) "下部"
    else "NA"
  }

  def noduleAreaTransformer(text: String) = {
    if (text.contains("表")) "表面"
    else if (text.contains("背")) "背侧"
    else if (text.contains("外")) "偏外侧"
    else if (text.contains("内")) "偏内侧"
    else "NA"
  }

  def revertStringIndex(text: String, lookUp: List[(String, String)]) = {
    if (lookUp.map(_._2).contains(text))
      lookUp.filter(z => {
      z._2 == text
    }).map(_._1).head else "NA"
  }

//  def radsTransformer(text: String): String = {
//      val radsPattern = new Regex("(.+\\W)(\\d[ABC]?)")
//      text match {
//        case radsPattern(x,y) => y.toString
//        case _ => "N/A"
//      }
//  }

}
