package com.philips.stemcell.clustering

import com.mongodb.spark.MongoSpark
import com.mongodb.spark._
import com.mongodb.spark.config._
import org.bson.Document
import org.bson.Document
import scala.collection.JavaConverters._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import com.philips.stemcell.jieba.wordSp
import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.clustering.LDA
import com.mongodb.spark.config._
import org.apache.spark.ml.feature.{CountVectorizer, HashingTF, IDF}
import org.apache.spark.ml.linalg.DenseVector

//case class clustering (
//                      Annotations: DataFrame,
//                      RawTexts: DataFrame,
//                      spark: SparkSession
//                      )

class documentsLDA(spark: SparkSession) {
  def preProcess(Annotations: DataFrame,
                 RawTexts: DataFrame,
                 schema: String,
                 modelPath: String = "./modelfiles/clusteringTransformer") = {
    val raw1 = new wordSp(spark).process(RawTexts, schema)
    raw1.show()
//    val hashingTF = new HashingTF()
//      .setInputCol("words").setOutputCol("rawFeatures").setNumFeatures(20)
    val vectorizer = new CountVectorizer().setInputCol("words").setOutputCol("rawFeatures")
    val idf = new IDF().setInputCol("rawFeatures").setOutputCol("features")
    val pipeline = new Pipeline().setStages(Array(vectorizer, idf))
    val transformerPipeline = pipeline.fit(raw1)
    transformerPipeline.write.overwrite().save(modelPath)
    val train = transformerPipeline.transform(raw1).select("raw_txt", "features", "md5")
    train.show()
    println("Data pre process is completed")
    train
  }

  def training(df: DataFrame,
               modelPath: String = "./modelfiles/documentsLDA") = {
    val lda = new LDA().setK(10).setMaxIter(400).setSeed(100)
    val model = lda.fit(df)

    model.write.overwrite().save(modelPath)

//    val ll = model.logLikelihood(df)
//    val lp = model.logPerplexity(df)
//    println(s"The lower bound on the log likelihood of the entire corpus: $ll")
//    println(s"The upper bound on perplexity: $lp")
//
//    // Describe topics.
//    val topics = model.describeTopics(3)
//    println("The topics described by their top-weighted terms:")
//    topics.show(false)


    // Shows the result.
    import spark.implicits._
    val transformed = model.setTopicDistributionCol("topic").transform(df)
    val result = transformed.rdd.map( x => {
      val raw_txt = x.getString(0)
      val md5 = x.getString(2)
      val topics = x.getAs[DenseVector](3).values
      val topic = topics.indexOf(topics.max)
      (raw_txt, md5, topic)
    }).toDF("raw_txt", "md5", "topic")
    result.show()
    result
  }

  def writeMongo(result: DataFrame,
                 uriName: String,
                 dbName: String,
                 collName: String
                ) = {
    val writeConfig = WriteConfig(Map("uri" -> uriName,"database" -> dbName, "collection" -> collName, "writeConcern.w" -> "majority"), Some(WriteConfig(spark)))

//    val result1 = result.withColumn("schema_name", when(col("topic") === 9, "hccOperation").otherwise("thyroidUS"))
    MongoSpark.save(result.write.option("collection", "test").mode("overwrite"), writeConfig)
    println("topic has been saved to Mongo")
  }
}
