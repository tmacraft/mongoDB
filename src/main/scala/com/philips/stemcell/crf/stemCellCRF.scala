package com.philips.stemcell.crf

import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.UUID.randomUUID

import com.mongodb.spark.config._
import com.mongodb.spark.MongoSpark
import com.mongodb.spark.config.WriteConfig
import com.philips.stemcell.utilities.normalization
import nlp.{CRF, CRFModel, Sequence, VerboseLevel1}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.bson.Document

import scala.collection.JavaConverters._
import scala.collection.immutable.{List, Map}
//import scala.collection.mutable._
//import scala.util.matching.Regex


class stemCellCRF(spark: SparkSession) extends normalization {

  def preProcessing(
                     raw: DataFrame,
                     annotation: DataFrame
                   ) = {

    println("Raw dataframe show")
    raw.show()

    println("Annotation dataframe show")
    annotation.show()

//    /**
//      * Extract fields from annotation dataframe
//      */
//
//    val at = annotation.select(
//      col("quote"),
//      col("tags").getItem(0).alias("tag"),
//      col("ranges").getItem(0)("endOffset").alias("end"),
//      col("ranges").getItem(0)("startOffset").alias("start"),
//      col("referer_url")
//    ).withColumn(
//      "length", col("end") - col("start")
//    )

    /**
      * Filter out fields not suit for CRF training
      */
    val atFiltered = annotation.filter(col("start") =!= -1)

    atFiltered.show()

    import spark.implicits._

    /**
      * Format annotation training dataframe
      */

    val at1 = atFiltered.rdd.map(
      x => {
        val length = x.getAs[Int](6)
        val quote = x.getString(0)
        val tag = x.getString(1)
        val seq = List.range(1, length + 1)
        val start = x.getAs[Int](3)
        val end = x.getAs[Int](2)
        val md5 = x.getString(4).split("md5=")(1)
        (quote, tag, length, seq, start, end, md5)
      }
    ).toDF("quote", "tag", "length", "seq", "start", "end", "md5")

    val atFormatted = at1
      .withColumn("place", explode($"seq"))
      .withColumn("pos", when($"place" === 1, "B").otherwise("I"))
      .withColumn("label", concat($"pos", lit("&"), $"tag"))
      .dropDuplicates()
      .orderBy("start", "place")
      .withColumn("position", $"start" + $"place" - 1)
      .select("position", "label", "md5")

//    println("Textb dataframe show")
//    at2.show()

    /**
      * Filter out raw texts don't have annotation records yet
      */
    val md5s = atFormatted.select("md5").distinct().map(_.getString(0)).collect().toList

    val rawFiltered = raw.filter(col("md5").isin(md5s:_*))

    /**
      * Format raw sentence dataframe
      */
    val raw1 = rawFiltered.rdd.map(
      x => {
        val md5 = x.getString(1)
        val sentence = x.getString(2).toList.map(_.toString)
        (md5, sentence)
      }
    ).toDF("md5", "sentence")
      .withColumn("sentence", explode($"sentence"))

    val raw2 = raw1.rdd.zipWithIndex().map(
      x => {
        val md5 = x._1.getString(0)
        val sentenceRaw = x._1.getString(1)
        val sentence = if (sentenceRaw.trim == "") "O" else sentenceRaw.trim
        val id = x._2
        (id, md5, sentence)
      }
    ).toDF("id", "md5", "sentence")

    val w = Window.partitionBy("md5").orderBy($"id")

    val rawFormatted = raw2.withColumn("rowID", rank().over(w)-1)
    println("Formatted raw dataframe show")
    rawFormatted.show()
    rawFormatted.printSchema()

    /**
      * Join raw sentence dataframe and annotation dataframe to get training rdd
      */
    val train = atFormatted.join(
      rawFormatted,
      (atFormatted.col("position") === rawFormatted.col("rowID")) && atFormatted.col("md5") === rawFormatted.col("md5"),
      "right").drop(atFormatted.col("md5"))
      .na.fill("O", Seq("label"))
      .withColumn("sentence", when(trim(col("sentence")) === "", "O").otherwise(col("sentence")))
      .select("sentence", "label", "id", "md5", "rowID")

    println("Train dataframe show")
    train.orderBy("md5", "rowID").show()

    println("Non O label result show")
    train.filter(col("label") =!= "O").show()

    /**
      * Format train RDD
      */

    val train1 = train.select("sentence", "label", "md5", "rowID")
      .rdd.map(r => (r(0).asInstanceOf[String], r.getString(1), r.getString(2), r.getAs[Int](3)))

    val trainFormatted = train1
      .filter(x => x._3.nonEmpty && x._3 != "")
      .groupBy(_._3)
      .map(x => {
      val list = x._2.toList.sortBy(_._4)
//      val id = list.map(_._4).head
      val label = list.map(y => {
        val concat = y._2 + "|--|" + y._1
        concat
      }).mkString("\t")
      label
    })

//    trainFormatted.foreach(
//      println(_)
//    )

    trainFormatted
  }

  def training(train: RDD[String]) = {
//    val actorSystem = ActorSystem()
//    implicit val executor = actorSystem.dispatcher
    val trainRDD = train.filter(_.nonEmpty).map(Sequence.deSerializer)
    println("trainRDD is constructed")
    val testRDD = train.filter(_.nonEmpty).map(Sequence.deSerializer)
    println("testRDD is constructed")
    val templates: Array[String] = scala.io.Source.fromFile("./data/template").getLines().filter(_.nonEmpty).toArray
    println("templates is loaded")
    println("Training the model")
    val model: CRFModel = CRF.train(templates, trainRDD, 0.25, 1, 300, 1E-3, "L2")
    println("CRF model training has been completed")

    val results: RDD[Sequence] = model.setNBest(10)
      .setVerboseMode(VerboseLevel1)
      .predict(testRDD)

    val score = results
      .zipWithIndex()
      .map(_.swap)
      .join(testRDD.zipWithIndex().map(_.swap))
      .map(_._2)
      .map(x => x._1.compare(x._2))
      .reduce(_ + _)
    val total = testRDD.map(_.toArray.length).reduce(_ + _)
    println(s"Prediction Accuracy: $score / $total")

    val accuracy = score.toFloat / total
    (model, accuracy)

  }

  def saveCRFModel(model: CRFModel, accuracy:Float, modelName: String)={
    if (accuracy > 0.7) {
      new java.io.File("./modelfiles").mkdir()
      //    //model save as String
      new java.io.PrintWriter(new FileOutputStream(s"./modelfiles/$modelName", false)) {
        write(CRFModel.save(model)); close()
      }
      println("Modelfile has been saved")
      //      val modelFromFile1 = CRFModel.load(scala.io.Source.fromFile("./modelFiles/model1").getLines().toArray.head)
    }
    else println("Model's accuracy is below 70%")
  }

  def pseudoLabeling(
                      train: DataFrame,
                      schemaV: String,
                      mongoURI: String = "mongodb://130.147.249.29",
//                      mongoURI: String = "mongodb://localhost",
//                      hostIP: String = "http://localhost",
                      hostIP: String = "mongodb://130.147.249.29",
                      modelName: String = "crfThyroid"
                    ) = {

    val model = CRFModel.load(scala.io.Source.fromFile(s"./modelfiles/$modelName").getLines().toArray.head)

    val trainRDDa = train.select("raw_txt", "md5")
      .rdd
      .map(x => {
        val token = x.getString(0).toList.map(x => {
          val token1 = x.toString
          val token2 = if (token1.trim == "" || token1.isEmpty) "0" else token1
          val train = "0|--|" + token2
          train
        }).mkString("\t")
        val md5 = x.getString(1)
        (token, md5)
        //        val token1 = if (token.trim == "" || token.isEmpty) "0" else token
        //        val train = "0|--|" + token1
        //        train.mkString("\t")
      })

    val md5Id = trainRDDa.map(_._2).zipWithIndex().keyBy(_._2)
    val trainRDD = trainRDDa.map(_._1)
//    trainRDD.foreach(
//      println(_)
//    )

    val result = model
      .setNBest(10)
      .setVerboseMode(VerboseLevel1)
      .predict(trainRDD.filter(_.nonEmpty).map(Sequence.deSerializer))

    //    result.foreach(
    //      println(_)
    //    )
    import spark.implicits._

    val result1 = result.map(x => {
      val probRaw = x.probPrinter().substring(5, 10)
      val prob = if (probRaw.startsWith("1.0")) 1.0 else probRaw.toDouble
      val token = x.toArray.map(_.tags(0))
      val label = x.toArray.map(_.label.trim)
      val zipped = token.zip(label)
      val zippedId = zipped.zipWithIndex.map(x => {
        val token = x._1._1
        val label = x._1._2.trim
        val pos = label.take(1)
        val element = label.split("&")(if (pos == "O") 0 else 1)
        val id = x._2
        Map("token" -> token, "pos" -> pos, "element" -> element, "id" -> id)
      }).filter(_ ("pos") != "O")
      (prob, zippedId)
    })

    val join_result1 = result1.zipWithIndex().keyBy(_._2)

    val result1A = join_result1.join(md5Id).map( x => {
      val md5 = x._2._2._1
      val prob = x._2._1._1._1
      val zippedId = x._2._1._1._2
      (prob, zippedId, md5)
    })
      .filter(_._2.nonEmpty)
//      .filter(x => x._1 >= 0.50 && x._2.nonEmpty)


    println("result1 complete")

    val epoch = System.currentTimeMillis()
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val updatedV = format.format(epoch).toString

    val result2 = result1A.flatMap(x => {

      val md5 = x._3
      val pos = x._2.map(y => (y("pos"), y("id"))).toList.asInstanceOf[List[Any]]
      val element = x._2.map(y => (y("element"), y("id"))).toList.asInstanceOf[List[Any]]
      val prev_value = (Map("pos" -> "O", "id" -> 999999999) +: x._2.dropRight(0)).map(y => (y("pos"), y("id"))).toList.asInstanceOf[List[Any]]
      val next_value = (x._2.drop(1) :+ Map("pos" -> "O", "id" -> 999999999)).map(y => (y("pos"), y("id"))).toList.asInstanceOf[List[Any]]
      val prev_value1 = (Map("element" -> "O", "id" -> 999999999) +: x._2.dropRight(0)).map(y => (y("element"), y("id"))).toList.asInstanceOf[List[Any]]
      val next_value1 = (x._2.drop(1) :+ Map("element" -> "O", "id" -> 999999999)).map(y => (y("element"), y("id"))).toList.asInstanceOf[List[Any]]
      val diff = (pos, prev_value, next_value).zipped.toList
      val diff1 = (element, prev_value1, next_value1).zipped.toList

      val diffA = diff.map(y => {
        val id = y._1.asInstanceOf[(String, Int)]._2
        val ind = if (y._1.asInstanceOf[(String, Int)]._1 == "O") "N" else "Y"
        (id, ind)
      }).filter(y => y._2 == "Y").map(_._1)

      val diffB = diff1.map(y => {
        val id = y._1.asInstanceOf[(String, Int)]._2
        val ind = if ((y._1.asInstanceOf[(String, Int)]._1 == y._2.asInstanceOf[(String, Int)]._1) || (y._1.asInstanceOf[(String, Int)]._1 == y._3.asInstanceOf[(String, Int)]._1)) "Y" else "N"
        (id, ind)
      }).filter(y => y._2 == "Y").map(_._1)

      val filtered = x._2
        .filter(y => diffA.contains(y("id")))
        .filter(y => diffB.contains(y("id")))
        .zipWithIndex
        .map(z => {
          val element = z._1("element")
          val group = z._1("id").asInstanceOf[Int] - z._2
          val token = z._1("token")
          val pos = z._1("pos")
          val id = z._1("id")
          (element, group, token, pos, id)
        })
        .groupBy(x => (x._1, x._2))
        .map(x => {
        val element = x._1._1
        val pos = x._2.map(_._4.toString)
        val token = x._2.map(_._3.toString)
        val id = x._2.map(_._5.toString.toInt)
        val startOffset = id.head
        val endOffset = id.last
        (element, token, pos, startOffset, endOffset)
      }).filter(x => x._3.head == "B")
        .map( f => {
          val label = f._1.toString
          val quote = f._2.mkString("")
          val startOffset = f._4.toString
          val endOffset = f._5.toString
          (label, quote, startOffset, endOffset, md5)
        })


      val norm = new normalization()

      val filtered1 = filtered.toSeq
      val filtered2 = filtered1.map( x => {
        val updated = updatedV
        val element = x._1.toString
//        val tags: WrappedArray[String] = WrappedArray.make(Array(element))
        val tags = Seq(element)
        val quote = x._2.mkString("")
        val created = updatedV
        val ranges = Seq(("/div[2]", "/div[2]", x._3.toInt.toString, (x._4.toInt+1).toString))
        val referer_url = s"$hostIP:6543/iCMC/thinking/test?src=websql&schema=$schemaV&md5=$md5"
        val annotator_schema_version = "v1.0"
//        val norm = spark.sparkContext.broadcast(normalization(quote, element))
        val text = norm.normalization(quote, element)
        val element_id = element
        val schema = schemaV
        val id = "stemCell"+randomUUID().toString
        (updated, tags, quote, created, ranges, referer_url, annotator_schema_version, text, element_id, schema, id)
      })

      filtered2
      })

    import spark.implicits._
    val resultDF = result2.toDF("updated", "tags", "quote", "created", "ranges", "referer_url", "annotator_schema_version", "text", "element_id", "schema", "id")
    resultDF.show()
//    val resultDF1 = resultDF.withColumn(
//      "ranges",
//      struct(
//        col("ranges")(0)("_1").as("start"),
//        col("ranges")(0)("_2").as("end"),
//        col("ranges")(0)("_3").cast(IntegerType).as("startOffset"),
//        col("ranges")(0)("_4").cast(IntegerType).as("endOffset")
//      )
//    )

    val rangesSchema = "array<struct<start:string,end:string,startOffset:integer,endOffset:integer>>"
    val resultDF1 = resultDF.withColumn("ranges", col("ranges").cast(rangesSchema))

    println("ready to save")

    val writeConfig = WriteConfig(Map("uri" -> mongoURI,"database" -> "AnnotatorJS", "collection" -> "plCRF", "writeConcern.w" -> "majority"), Some(WriteConfig(spark)))
//    MongoSpark.save(resultDF.write.option("collection", "pseudoLabel").mode("overwrite"), writeConfig)
    MongoSpark.save(resultDF1.write.mode("overwrite"), writeConfig)

    val plThyroid = resultDF1.select(
      col("quote"),
      col("tags").getItem(0).alias("tag"),
      col("ranges").getItem(0)("endOffset").alias("end"),
      col("ranges").getItem(0)("startOffset").alias("start"),
      col("referer_url"),
      col("text")
    ).withColumn(
      "length", col("end") - col("start")
    )

    plThyroid
  }

  def writeMongo(result: DataFrame,
                 uriName: String = "mongodb://localhost",
                 dbName: String = "AnnotatorJS",
                 collName: String
                ) = {
    val writeConfig = WriteConfig(Map("uri" -> uriName,"database" -> dbName, "collection" -> collName, "writeConcern.w" -> "majority"), Some(WriteConfig(spark)))

    //    val result1 = result.withColumn("schema_name", when(col("topic") === 9, "hccOperation").otherwise("thyroidUS"))
    MongoSpark.save(result.write.option("collection", "test").mode("overwrite"), writeConfig)
    println("topic has been saved to Mongo")
  }

}




