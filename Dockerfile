# Pull base image
#FROM openjdk:jre-alpine
#FROM frolvlad/alpine-glibc:alpine-3.8

FROM anapsix/alpine-java

# Define env variables
ENV SBT_VERSION  0.13.11
ENV SBT_HOME /usr/local/sbt
ENV PATH ${PATH}:${SBT_HOME}/bin

# Install sbt
RUN apk add --update curl ca-certificates bash && \
    curl -sL "http://dl.bintray.com/sbt/native-packages/sbt/$SBT_VERSION/sbt-$SBT_VERSION.tgz" | gunzip | tar -x -C /usr/local && \
    echo -ne "- with sbt $SBT_VERSION\n" >> /root/.built &&\
    apk del curl

# Define working directory
WORKDIR /

# Copy to directory
COPY . .

## Build to fat jar
#RUN sbt assembly


# Define entrypoint
#ENTRYPOINT ["/entrypoint.sh"]

CMD ["java", "-Xms1g", "-Xmx15g", "-jar", "target/scala-2.11/mongo.scala-assembly-0.1.jar"]

